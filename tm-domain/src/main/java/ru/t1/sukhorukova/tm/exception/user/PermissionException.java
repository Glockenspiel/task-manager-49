package ru.t1.sukhorukova.tm.exception.user;

public final class PermissionException extends AbstractUserException {

    public PermissionException() {
        super("Error! Access denied...");
    }

}
