package ru.t1.sukhorukova.tm.exception.field;

public final class UserIdEmptyException extends AbstractFieldExceprion {

    public UserIdEmptyException() {
        super("Error! User Id is empty...");
    }

}
