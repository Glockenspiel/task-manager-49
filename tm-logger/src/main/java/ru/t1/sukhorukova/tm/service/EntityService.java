package ru.t1.sukhorukova.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.LinkedHashMap;
import java.util.Map;

public final class EntityService {

    private ObjectMapper objectMapper = new YAMLMapper();

    @SneakyThrows
    public void log(@NotNull final String yml) {
        @NotNull final Map<String, Object> event = objectMapper.readValue(yml, LinkedHashMap.class);
        @NotNull final String fileName = event.get("table").toString() + ".yml";
        @NotNull final byte[] bytes = yml.getBytes();
        @NotNull final File file = new File(fileName);
        if (!file.exists()) file.createNewFile();
        Files.write(Paths.get(fileName), bytes, StandardOpenOption.APPEND);
    }

}
