package ru.t1.sukhorukova.tm.listener;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

public final class EntityListener implements MessageListener {

    private final ru.t1.sukhorukova.tm.service.EntityService loggerService;

    public EntityListener(final ru.t1.sukhorukova.tm.service.EntityService loggerService) {
        this.loggerService = loggerService;
    }

    @Override
    @SneakyThrows
    public void onMessage(Message message) {
        @NotNull final boolean checkType = message instanceof TextMessage;
        if (!checkType) return;
        @NotNull final TextMessage textMessage = (TextMessage) message;
        @NotNull final String yaml = textMessage.getText();
        loggerService.log(yaml);
    }

}
