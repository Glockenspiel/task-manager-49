package ru.t1.vsukhorukova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.rules.ExpectedException;
import ru.t1.sukhorukova.tm.api.endpoint.IAuthEndpoint;
import ru.t1.sukhorukova.tm.api.endpoint.IUserEndpoint;
import ru.t1.sukhorukova.tm.api.service.IPropertyService;
import ru.t1.sukhorukova.tm.dto.request.user.*;
import ru.t1.sukhorukova.tm.dto.model.UserDTO;
import ru.t1.sukhorukova.tm.service.PropertyService;
import ru.t1.vsukhorukova.tm.marker.IntegrationCategory;

import static ru.t1.vsukhorukova.tm.constant.UserTestData.*;
import static ru.t1.vsukhorukova.tm.constant.UserTestData.USER_PASSWORD;

@Category(IntegrationCategory.class)
public final class UserEndpointTest {

    @Nullable
    private String adminToken;

    @Nullable
    private String userToken;

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(propertyService);

    @NotNull
    private final IUserEndpoint userEndpoint = IUserEndpoint.newInstance(propertyService);

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    private String getToken(@NotNull final String login, @NotNull final String password) {
        @NotNull final UserLoginRequest request = new UserLoginRequest(null);
        request.setLogin(login);
        request.setPassword(password);

        @Nullable final String token = authEndpoint.login(request).getToken();
        return token;
    }

    @Before
    public void login() {
        adminToken = getToken(ADMIN_LOGIN, ADMIN_PASSWORD);
        userToken = getToken(USER_LOGIN, USER_PASSWORD);
    }

    @After
    public void logout() {
        authEndpoint.logout(new UserLogoutRequest(adminToken));
        authEndpoint.logout(new UserLogoutRequest(userToken));
        userToken = null;
        adminToken = null;
    }

    @Test
    public void viewProfileUser() {
        @NotNull final UserViewProfileRequest request = new UserViewProfileRequest(userToken);
        @Nullable final UserDTO user = userEndpoint.viewProfileUser(request).getUser();
        Assert.assertNotNull(user);
        Assert.assertEquals(USER_LOGIN, user.getLogin());

        @NotNull final UserViewProfileRequest requestException = new UserViewProfileRequest(null);
        thrown.expect(Exception.class);
        userEndpoint.viewProfileUser(requestException);
    }

    @Test
    public void updateProfileUser() {
        @NotNull final String firstName = "FIRST_NAME";
        @NotNull final String lastName = "LAST_NAME";
        @NotNull final String middleName = "MIDDLE_NAME";

        @NotNull final UserUpdateProfileRequest requestUpdate = new UserUpdateProfileRequest(userToken);
        requestUpdate.setFirstName(firstName);
        requestUpdate.setLastName(lastName);
        requestUpdate.setMiddleName(middleName);
        @NotNull UserDTO user = userEndpoint.updateProfileUser(requestUpdate).getUser();
        Assert.assertNotNull(user);
        Assert.assertEquals(firstName, user.getFirstName());
        Assert.assertEquals(lastName, user.getLastName());
        Assert.assertEquals(middleName, user.getMiddleName());

        @NotNull final UserViewProfileRequest requestView = new UserViewProfileRequest(userToken);
        user = userEndpoint.viewProfileUser(requestView).getUser();
        Assert.assertNotNull(user);
        Assert.assertEquals(firstName, user.getFirstName());
        Assert.assertEquals(lastName, user.getLastName());
        Assert.assertEquals(middleName, user.getMiddleName());

        @NotNull final UserUpdateProfileRequest requestUpdateNull = new UserUpdateProfileRequest(userToken);
        userEndpoint.updateProfileUser(requestUpdate);

        @NotNull final UserUpdateProfileRequest requestException = new UserUpdateProfileRequest();
        thrown.expect(Exception.class);
        userEndpoint.updateProfileUser(requestException);
    }

    @Test
    public void changePasswordUser() {
        @NotNull final String user2Token = getToken(USER2_LOGIN, USER2_PASSWORD);

        @NotNull final UserViewProfileRequest requestView = new UserViewProfileRequest(user2Token);
        @Nullable UserDTO user = userEndpoint.viewProfileUser(requestView).getUser();
        @NotNull final String oldPasswordHash = user.getPasswordHash();

        @NotNull final UserChangePasswordRequest requestChangePassword = new UserChangePasswordRequest(user2Token);
        @NotNull final String newPassword = "NEW_PASSWORD";
        requestChangePassword.setPassword(newPassword);
        user = userEndpoint.changePasswordUser(requestChangePassword).getUser();
        Assert.assertNotNull(user);
        Assert.assertEquals(USER2_LOGIN, user.getLogin());
        Assert.assertNotEquals(oldPasswordHash, user.getPasswordHash());

        user = userEndpoint.viewProfileUser(requestView).getUser();
        Assert.assertNotNull(user);
        Assert.assertEquals(USER2_LOGIN, user.getLogin());
        Assert.assertNotEquals(oldPasswordHash, user.getPasswordHash());

        requestChangePassword.setPassword(USER2_PASSWORD);
        userEndpoint.changePasswordUser(requestChangePassword);

        @NotNull final UserChangePasswordRequest requestException = new UserChangePasswordRequest();
        thrown.expect(Exception.class);
        userEndpoint.changePasswordUser(requestException);
    }

    @Test
    public void lockUnlockUser() {
        @NotNull String token = getToken(USER2_LOGIN, USER2_PASSWORD);
        Assert.assertNotNull(token);

        @NotNull final UserLockRequest requestLock = new UserLockRequest(adminToken);
        requestLock.setLogin(USER2_LOGIN);
        @Nullable UserDTO user = userEndpoint.lockUser(requestLock).getUser();
        Assert.assertNotNull(user);
        Assert.assertEquals(USER2_LOGIN, user.getLogin());
        Assert.assertEquals(true, user.getLocked());
        try {
            token = getToken(USER2_LOGIN, USER2_PASSWORD);
            Assert.fail("Expected Exception");
        } catch (Exception e) {}

        @NotNull final UserUnlockRequest requestUnlock = new UserUnlockRequest(adminToken);
        requestUnlock.setLogin(USER2_LOGIN);
        user = userEndpoint.unlockUser(requestUnlock).getUser();
        Assert.assertNotNull(user);
        Assert.assertEquals(USER2_LOGIN, user.getLogin());
        Assert.assertEquals(false, user.getLocked());

        token = getToken(USER2_LOGIN, USER2_PASSWORD);
        Assert.assertNotNull(token);
    }

    @Test(expected = Exception.class)
    public void lockUserAccessDenied() {
        @NotNull final UserLockRequest requestException = new UserLockRequest(userToken);
        requestException.setLogin(USER2_LOGIN);
        userEndpoint.lockUser(requestException);
    }

    @Test(expected = Exception.class)
    public void unlockUserAccessDenied() {
        @NotNull final UserUnlockRequest requestException = new UserUnlockRequest(userToken);
        requestException.setLogin(USER2_LOGIN);
        userEndpoint.unlockUser(requestException);
    }

    @Test
    public void registryRemoveUser() {
        @NotNull final String login = "TEST_LOGIN";
        @NotNull final String password = "TEST_PASSWORD";
        @NotNull final String email = "test@address.ru";

        @NotNull final UserRegistryRequest requestRegistry = new UserRegistryRequest();
        requestRegistry.setLogin(login);
        requestRegistry.setPassword(password);
        requestRegistry.setEmail(email);
        @Nullable UserDTO user = userEndpoint.registryUser(requestRegistry).getUser();
        Assert.assertNotNull(user);
        Assert.assertEquals(login, user.getLogin());
        Assert.assertEquals(email, user.getEmail());

        @NotNull final String token = getToken(login, password);
        @NotNull final UserViewProfileRequest requestView = new UserViewProfileRequest(token);
        user = userEndpoint.viewProfileUser(requestView).getUser();
        Assert.assertNotNull(user);
        Assert.assertEquals(login, user.getLogin());
        Assert.assertEquals(email, user.getEmail());

        @NotNull final UserRemoveRequest requestRemove = new UserRemoveRequest(adminToken);
        requestRemove.setLogin(login);
        user = userEndpoint.removeUser(requestRemove).getUser();
        Assert.assertNotNull(user);
        Assert.assertEquals(login, user.getLogin());
        Assert.assertEquals(email, user.getEmail());

        thrown.expect(Exception.class);
        getToken(login, password);
        @NotNull final UserRegistryRequest requestRegistryException = new UserRegistryRequest(userToken);
        thrown.expect(Exception.class);
        userEndpoint.registryUser(requestRegistryException);
        @NotNull final UserRemoveRequest requestRemoveException = new UserRemoveRequest(userToken);
        thrown.expect(Exception.class);
        userEndpoint.removeUser(requestRemoveException);
    }

    @Test(expected = Exception.class)
    public void removeUserAccessDenied() {
        @NotNull final UserRemoveRequest requestException = new UserRemoveRequest(userToken);
        requestException.setLogin(USER2_LOGIN);
        userEndpoint.removeUser(requestException);
    }

}
