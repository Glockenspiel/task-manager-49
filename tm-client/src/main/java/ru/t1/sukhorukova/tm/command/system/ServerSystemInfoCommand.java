package ru.t1.sukhorukova.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.t1.sukhorukova.tm.dto.request.system.SystemInfoRequest;
import ru.t1.sukhorukova.tm.dto.response.system.SystemInfoResponse;

public final class ServerSystemInfoCommand extends AbstractSystemCommand {

    @NotNull
    public static final String ARGUMENT = "-is";

    @NotNull
    public static final String NAME = "server-info";

    @NotNull
    public static final String DESCRIPTION = "Show server system information.";

    @Override
    public void execute() {
        System.out.println("[INFO]");

        @NotNull final SystemInfoResponse response = getSystemEndpoint().getSystemInfo(new SystemInfoRequest());
        final int availableProcessors = response.getAvailableProcessors();
        @NotNull final String freeMemoryFormat = response.getFreeMemoryFormat();
        @NotNull final String maxMemoryValue = response.getMaxMemoryValue();
        @NotNull final String totalMemoryFormat = response.getTotalMemoryFormat();
        @NotNull final String usageMemoryFormat = response.getUsageMemoryFormat();

        System.out.println("Available processors: " + availableProcessors + " cores");
        System.out.println("Free memory: " + freeMemoryFormat);
        System.out.println("Maximum memory: " + maxMemoryValue);
        System.out.println("Total memory: " + totalMemoryFormat);
        System.out.println("Usage memory: " + usageMemoryFormat);
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
