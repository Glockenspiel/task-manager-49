package ru.t1.sukhorukova.tm.service;

import lombok.Getter;
import org.hibernate.event.service.spi.EventListenerRegistry;
import org.hibernate.event.spi.EventType;
import org.hibernate.internal.SessionFactoryImpl;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.api.service.IConnectionService;
import ru.t1.sukhorukova.tm.api.service.ILoggerService;
import ru.t1.sukhorukova.tm.listener.EntityListener;
import ru.t1.sukhorukova.tm.listener.JmsLoggerProducer;

import javax.persistence.EntityManagerFactory;
import java.io.IOException;
import java.util.logging.*;

public final class LoggerService implements ILoggerService {

    @NotNull
    private final String configFile = "/logger.properties";

    @NotNull
    private final String commands = "COMMANDS";

    @NotNull
    private final String commandsFile = "./commands.xml";

    @NotNull
    private final String errors = "ERRORS";

    @NotNull
    private final String errorsFile = "./errors.xml";

    @NotNull
    private final String messages = "MESSAGES";

    @NotNull
    private final String messagesFile = "./messages.xml";

    @NotNull
    private final LogManager manager = LogManager.getLogManager();

    @NotNull
    private final Logger loggerRoot = Logger.getLogger("");

    @NotNull
    @Getter
    private final Logger loggerCommand = Logger.getLogger(commands);

    @NotNull
    @Getter
    private final Logger loggerError = Logger.getLogger(errors);

    @NotNull
    @Getter
    private final Logger loggerMessage = Logger.getLogger(messages);

    @NotNull
    private final ConsoleHandler consoleHandler = getConsoleHandler();

    @NotNull
    private final IConnectionService connectionService;

    private @NotNull ConsoleHandler getConsoleHandler() {
        @NotNull final ConsoleHandler handler = new ConsoleHandler();
        handler.setFormatter(new Formatter() {
            @Override
            public String format(LogRecord record) {
                return record.getMessage() + "\n";
            }
        });
        return handler;
    }

    public LoggerService(final IConnectionService connectionService) {
        this.connectionService = connectionService;
        loadConfigFromFile();
        registry(loggerCommand, commandsFile, false);
        registry(loggerError, errorsFile, true);
        registry(loggerMessage, messagesFile, true);
    }

    private void loadConfigFromFile() {
        try {
            manager.readConfiguration(LoggerService.class.getResourceAsStream(configFile));
        } catch (@NotNull final IOException e) {
            loggerRoot.severe(e.getMessage());
        }
    }

    private void registry(
            @NotNull final Logger logger,
            @Nullable final String fileName,
            final boolean isConsole
    ) {
        try {
            if (isConsole) logger.addHandler(consoleHandler);
            logger.setUseParentHandlers(false);
            if (fileName != null && !fileName.isEmpty())
                logger.addHandler(new FileHandler(fileName));
        } catch (@NotNull final IOException e) {
            loggerRoot.severe(e.getMessage());
        }
    }

    @Override
    public void info(@Nullable final String message) {
        if (message == null || message.isEmpty()) return;
        loggerMessage.info(message);
    }

    @Override
    public void debug(@Nullable final String message) {
        if (message == null || message.isEmpty()) return;
        loggerMessage.fine(message);
    }

    @Override
    public void command(@Nullable final String message) {
        if (message == null || message.isEmpty()) return;
        loggerCommand.fine(message);
    }

    @Override
    public void error(@Nullable final Exception e) {
        if (e == null) return;
        loggerError.log(Level.SEVERE, e.getMessage(), e);
    }

    @Override
    public void initJmsLogger() {
        @NotNull final JmsLoggerProducer jmsLoggerProducer = new JmsLoggerProducer();
        @NotNull final EntityListener entityListener = new EntityListener(jmsLoggerProducer);
        @NotNull final EntityManagerFactory entityManagerFactory = connectionService.getEntityManagerFactory();
        @NotNull final SessionFactoryImpl sessionFactory = entityManagerFactory.unwrap(SessionFactoryImpl.class);
        @NotNull final EventListenerRegistry eventListenerRegistry = sessionFactory.getServiceRegistry().getService(EventListenerRegistry.class);
        eventListenerRegistry.getEventListenerGroup(EventType.POST_INSERT).appendListener(entityListener);
        eventListenerRegistry.getEventListenerGroup(EventType.POST_DELETE).appendListener(entityListener);
        eventListenerRegistry.getEventListenerGroup(EventType.POST_UPDATE).appendListener(entityListener);
    }

}
