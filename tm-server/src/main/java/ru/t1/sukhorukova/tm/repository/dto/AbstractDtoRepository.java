package ru.t1.sukhorukova.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.sukhorukova.tm.api.repository.dto.IDtoRepository;
import ru.t1.sukhorukova.tm.dto.model.AbstractModelDTO;

import javax.persistence.EntityManager;

public abstract class AbstractDtoRepository<M extends AbstractModelDTO> implements IDtoRepository<M> {

    @NotNull
    protected final EntityManager entityManager;

    protected AbstractDtoRepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void add(@NotNull final M model) {
        entityManager.persist(model);
    }

    @Override
    public void update(@NotNull final M model) {
        entityManager.merge(model);
    }

    @Override
    public void remove(@NotNull final M model) {
        entityManager.remove(model);
    }

}
