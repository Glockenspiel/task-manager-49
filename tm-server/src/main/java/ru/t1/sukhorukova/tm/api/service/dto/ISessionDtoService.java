package ru.t1.sukhorukova.tm.api.service.dto;

import ru.t1.sukhorukova.tm.dto.model.SessionDTO;

public interface ISessionDtoService extends IUserOwnedDtoService<SessionDTO> {
}
