package ru.t1.sukhorukova.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.sukhorukova.tm.model.Session;

public interface ISessionRepository extends IUserOwnerRepository<Session> {

    void removeAll();

    void removeAll(@NotNull String userId);

}
