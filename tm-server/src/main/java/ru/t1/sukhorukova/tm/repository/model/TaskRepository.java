package ru.t1.sukhorukova.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.api.repository.model.ITaskRepository;
import ru.t1.sukhorukova.tm.enumerated.TaskSort;
import ru.t1.sukhorukova.tm.model.Session;
import ru.t1.sukhorukova.tm.model.Task;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;

public final class TaskRepository extends AbstractUserOwnerRepository<Task> implements ITaskRepository {

    @NotNull
    public TaskRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Nullable
    @Override
    public List<Task> findAll() {
        @NotNull final String jpql = "SELECT m FROM Task m";
        return entityManager.createQuery(jpql, Task.class).getResultList();
    }

    @Nullable
    @Override
    public List<Task> findAll(@NotNull final String userId) {
        @NotNull final String jpql = "SELECT m FROM Task m WHERE m.user.id = :userId";
        return entityManager.createQuery(jpql, Task.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public List<Task> findAll(@NotNull final String userId, @NotNull final TaskSort sort) {
        @NotNull final String jpql = "SELECT m FROM Task m WHERE m.user.id = :userId ORDER BY " + sort.getColumnName();
        return entityManager.createQuery(jpql, Task.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    public List<Task> findAllByProjectId(
            @NotNull final String userId,
            @NotNull final String projectId
    ) {
        @NotNull final String jpql = "SELECT m FROM Task m WHERE m.user.id = :userId AND m.project.id = :projectId";
        return entityManager.createQuery(jpql, Task.class)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .getResultList();
    }

    @Nullable
    @Override
    public Task findOneById(@NotNull final String id) {
        return entityManager.find(Task.class, id);
    }

    @Nullable
    @Override
    public Task findOneById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final String jpql = "SELECT m FROM Task m WHERE m.id = :id AND m.user.id = :userId";
        return entityManager.createQuery(jpql, Task.class)
                .setParameter("id", id)
                .setParameter("userId", userId)
                .setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final Task task) {
        @NotNull final String jpql = "DELETE FROM Task m WHERE m.id = :taskId AND m.user.id = :userId";
        entityManager.createQuery(jpql)
                .setParameter("taskId", task.getId())
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    public void removeOneById(@NotNull final String id) {
        Optional<Task> model = Optional.ofNullable(findOneById(id));
        model.ifPresent(this::remove);
    }

    @Override
    public void removeOneById(@NotNull final String userId, @NotNull final String id) {
        Optional<Task> model = Optional.ofNullable(findOneById(userId, id));
        model.ifPresent(this::remove);
    }

    @Override
    public void removeAll() {
        @NotNull final String jpql = "DELETE FROM Task";
        entityManager.createQuery(jpql).executeUpdate();
    }

    @Override
    public void removeAll(@NotNull final String userId) {
        @NotNull final String jpql = "DELETE FROM Task m WHERE m.user.id = :userId";
        entityManager.createQuery(jpql)
                .setParameter("userId", userId)
                .executeUpdate();
    }

    public void removeTasksByProjectId(
            @NotNull final String userId,
            @NotNull final String projectId
    ) {
        @NotNull final String jpql = "DELETE FROM Task m WHERE m.user.id = :userId AND m.project.id = :projectId";
        entityManager.createQuery(jpql)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .executeUpdate();
    }

    @Override
    public long getSize() {
        @NotNull final String jpql = "SELECT COUNT(m) FROM Task m";
        return entityManager.createQuery(jpql, Long.class).getSingleResult();
    }

    @Override
    public long getSize(@NotNull final String userId) {
        @NotNull final String jpql = "SELECT COUNT(m) FROM Task m WHERE m.user.id = :userId";
        return entityManager.createQuery(jpql, Long.class)
                .setParameter("userId", userId)
                .getSingleResult();
    }

}
