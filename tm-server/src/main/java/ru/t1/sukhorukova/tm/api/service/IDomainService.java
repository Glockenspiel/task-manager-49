package ru.t1.sukhorukova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.dto.Domain;

public interface IDomainService {

    @NotNull
    Domain getDomain();

    void setDomain(@Nullable Domain domain);

    void dataBackupLoad();

    void dataBackupSave();

    void dataBase64Load();

    void dataBase64Save();

    void dataBinaryLoad();

    void dataBinarySave();

    void dataJsonLoadFasterXml();

    void dataJsonLoadJaxB();

    void dataJsonSaveFasterXml();

    void dataJsonSaveJaxB();

    void dataXmlLoadFasterXml();

    void dataXmlLoadJaxB();

    void dataXmlSaveFasterXml();

    void dataXmlSaveJaxB();

    void dataYamlLoadFasterXml();

    void dataYamlSaveFasterXml();

}
